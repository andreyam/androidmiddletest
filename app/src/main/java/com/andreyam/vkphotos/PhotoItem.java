package com.andreyam.vkphotos;

/*
 * Created by Andrey on 23.07.2017.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;

class PhotoItem extends AbstractItem<PhotoItem, PhotoItem.ViewHolder> {

    public int id;
    public String name;
    private String date;
    private String imageUrl;
    private boolean isLoad;


    PhotoItem withImage(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    PhotoItem withName(String name) {
        this.name = name;
        return this;
    }

    PhotoItem withId(int id) {
        this.id = id;
        return this;
    }

    PhotoItem withDate(String date) {
        this.date = date;
        return this;
    }

    PhotoItem withIsLoad(boolean isLoad) {
        this.isLoad = isLoad;
        return this;
    }

    //The unique ID for this type of item
    @Override
    public int getType() {
        return R.id.fastadapter_photo_item_id;
    }

    //The layout to be used for this type of item
    @Override
    public int getLayoutRes() {
        return R.layout.photo_item;
    }

    //The logic to bind your data to the view
    @Override
    public void bindView(ViewHolder viewHolder, List<Object> payloads) {
        //call super so the selection is already handled for you
        super.bindView(viewHolder, payloads);

        //get the context
        Context ctx = viewHolder.itemView.getContext();
        //bind our data
        if (!isLoad) {
            viewHolder.load.setVisibility(View.INVISIBLE);
        }
        //load glide
        /*Glide.clear(viewHolder.imageView);
        Glide.with(ctx).load(imageUrl).animate(R.anim.alpha_on).into(viewHolder.imageView);*/

        Picasso.with(ctx)
                .load(imageUrl)
                .transform(new CropSquareTransformation())
                //.centerCrop()
                .into(viewHolder.imageView);

        //set the text for the name
        viewHolder.name.setText(name);
        //set the text for the description or hide
        viewHolder.date.setText(date);
    }

    private class CropSquareTransformation implements Transformation {
        @Override public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());
            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;
            Bitmap result = Bitmap.createBitmap(source, x, y, size, size);
            if (result != source) {
                source.recycle();
            }
            return result;
        }

        @Override public String key() { return "square()"; }
    }

    //reset the view here (this is an optional method, but recommended)
    @Override
    public void unbindView(ViewHolder holder) {
        super.unbindView(holder);
        holder.imageView.setImageDrawable(null);
        holder.name.setText(null);
        holder.date.setText(null);
    }

    //Init the viewHolder for this Item
    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    static class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout root_view;
        ProgressBar load;
        ImageView imageView;
        TextView name;
        TextView date;

        ViewHolder(View view) {
            super(view);

            this.root_view = (RelativeLayout) view.findViewById(R.id.root_container);

            Context ctx = view.getContext();
            int scr_width = ctx.getResources().getDisplayMetrics().widthPixels;
            int square = scr_width / 3;
            GridLayoutManager.LayoutParams layoutParams
                    = (GridLayoutManager.LayoutParams) root_view.getLayoutParams();
            layoutParams.width = square;
            layoutParams.height = square;
            root_view.setLayoutParams(layoutParams);

            this.load = (ProgressBar) view.findViewById(R.id.progress_load);
            this.imageView = (ImageView) view.findViewById(R.id.photo_img);
            this.name = (TextView) view.findViewById(R.id.photo_name);
            this.date = (TextView) view.findViewById(R.id.photo_date);
        }
    }
}