package com.andreyam.vkphotos;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.mikepenz.community_material_typeface_library.CommunityMaterial;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.iconics.IconicsDrawable;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiPhotoAlbum;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AlbumsActivity extends AppCompatActivity {

    int albums_count = 0;
    RecyclerView albumsList;
    FastItemAdapter<AlbumItem> fastAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_albums);

        albumsList = (RecyclerView) findViewById(R.id.albums_list);
        albumsList.setLayoutManager(new LinearLayoutManager(albumsList.getContext()));

        fastAdapter = new FastItemAdapter<>();

        fastAdapter.withSelectable(false);

        VKRequest requestAlbumsCount = new VKRequest("photos.getAlbumsCount", null);

        requestAlbumsCount.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);
                JSONObject jsonResponse = response.json;

                try {
                    albums_count = jsonResponse.getInt("response");

                    for (int i = 0; i < albums_count; i++) {
                        fastAdapter.add(new AlbumItem().withIsLoad(true).withName("Loading.."));
                    }

                    albumsList.setAdapter(fastAdapter);

                    // "photos.getAlbums"

                    VKRequest requestAlbums = new VKRequest("photos.getAlbums", VKParameters.from("need_covers", 1));

                    requestAlbums.executeWithListener(new VKRequest.VKRequestListener() {
                        @Override
                        public void onComplete(VKResponse response) {
                            super.onComplete(response);

                            JSONObject jsonResponse = response.json;
                            new getAlbums().execute(jsonResponse);
                        }

                        @Override
                        public void onError(VKError error) {
                            super.onError(error);
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            @Override
            public void onError(VKError error) {
                super.onError(error);
            }
        });

    }

    private class getAlbums extends AsyncTask<JSONObject, String, VKApiPhotoAlbum []>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            /*progress_dialog = new MaterialDialog.Builder(ResultActivity.this)
                    .title(R.string.dlg_wait_title)
                    .content(R.string.dlg_wait_msg)
                    .progress(true, 0)
                    .cancelable(false)
                    .build();
            progress_dialog.show();*/

        }

        @Override
        protected VKApiPhotoAlbum[] doInBackground(JSONObject... response) {

            VKApiPhotoAlbum [] albums = null;

            try {
                JSONArray jsonAlbums = response[0].getJSONObject("response").getJSONArray("items");
                albums = new VKApiPhotoAlbum[albums_count];
                for (int i = 0; i < albums_count; i++) {
                    JSONObject item = jsonAlbums.getJSONObject(i);
                    albums[i] = new VKApiPhotoAlbum(item);
                    /*Log.i("MyLog", "getAlbumsData | id - " + albums[i].id );
                    Log.i("MyLog", "getAlbumsData | thumb_id - " + albums[i].thumb_id);
                    Log.i("MyLog", "getAlbumsData | title - " + albums[i].title);
                    Log.i("MyLog", "getAlbumsData | description - " + albums[i].description);
                    Log.i("MyLog", "getAlbumsData | size - " + albums[i].size );
                    Log.i("MyLog", "getAlbumsData | thumb_src  - " + albums[i].thumb_src  );
                    Log.i("MyLog", "getAlbumsData | --------------------------");*/
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return albums;
        }

        @Override
        protected void onPostExecute(VKApiPhotoAlbum[] albums){

            fastAdapter.clear();

            for (VKApiPhotoAlbum album : albums) {
                int id = album.id;
                String name = album.title;
                String descr = album.description;
                String imgUrl = album.thumb_src;
                fastAdapter.add(new AlbumItem().withId(id)
                        .withName(name)
                        .withDescription(descr)
                        .withImage(imgUrl)
                        .withIsLoad(false));
            }

            albumsList.setAdapter(fastAdapter);

            fastAdapter.withOnClickListener(new FastAdapter.OnClickListener<AlbumItem>() {
                @Override
                public boolean onClick(View v, IAdapter<AlbumItem> adapter, AlbumItem item, int position) {
                    Intent view_album = new Intent(AlbumsActivity.this, AlbumDetailActivity.class);
                    view_album.putExtra("album_id", item.id);
                    startActivity(view_album);
                    return true;
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.albums_menu, menu);

        menu.findItem(R.id.menu_btn_logout)
                .setIcon(new IconicsDrawable(AlbumsActivity.this,
                CommunityMaterial.Icon.cmd_logout)
                        .color(Color.WHITE).sizeDp(18));

        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if ((item.getItemId() == R.id.menu_btn_logout)) {
            VKSdk.logout();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
