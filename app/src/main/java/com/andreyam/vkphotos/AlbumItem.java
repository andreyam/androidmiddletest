package com.andreyam.vkphotos;

/*
 * Created by Andrey on 19.07.2017.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

class AlbumItem extends AbstractItem<AlbumItem, AlbumItem.ViewHolder> {

    public int id;
    public String name;
    private String description;
    public String imageUrl;
    private boolean isLoad;

    AlbumItem withImage(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    AlbumItem withName(String name) {
        this.name = name;
        return this;
    }

    AlbumItem withId(int id) {
        this.id = id;
        return this;
    }

    AlbumItem withDescription(String description) {
        this.description = description;
        return this;
    }

    AlbumItem withIsLoad(boolean isLoad) {
        this.isLoad = isLoad;
        return this;
    }

    //The unique ID for this type of item
    @Override
    public int getType() {
        return R.id.fastadapter_album_item_id;
    }

    //The layout to be used for this type of item
    @Override
    public int getLayoutRes() {
        return R.layout.album_item;
    }

    //The logic to bind your data to the view
    @Override
    public void bindView(ViewHolder viewHolder, List<Object> payloads) {
        //call super so the selection is already handled for you
        super.bindView(viewHolder, payloads);

        //get the context
        Context ctx = viewHolder.itemView.getContext();
        //bind our data
        if (!isLoad) {
            viewHolder.load.setVisibility(View.INVISIBLE);
        }
        //load glide
        Glide.clear(viewHolder.imageView);
        Glide.with(ctx).load(imageUrl).animate(R.anim.alpha_on).into(viewHolder.imageView);
        //set the text for the name
        viewHolder.name.setText(name);
        //set the text for the description or hide
        viewHolder.description.setText(description);
    }

    //reset the view here (this is an optional method, but recommended)
    @Override
    public void unbindView(ViewHolder holder) {
        super.unbindView(holder);
        holder.imageView.setImageDrawable(null);
        holder.name.setText(null);
        holder.description.setText(null);
    }

    //Init the viewHolder for this Item
    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    static class ViewHolder extends RecyclerView.ViewHolder {
        ProgressBar load;
        ImageView imageView;
        protected TextView name;
        TextView description;

        ViewHolder(View view) {
            super(view);
            this.load = (ProgressBar) view.findViewById(R.id.progress_load);
            this.imageView = (ImageView) view.findViewById(R.id.album_cover);
            this.name = (TextView) view.findViewById(R.id.album_name);
            this.description = (TextView) view.findViewById(R.id.album_description);
        }
    }
}