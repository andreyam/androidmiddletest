package com.andreyam.vkphotos;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiPhoto;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class AlbumDetailActivity extends AppCompatActivity {

    int album_id, photo_count;
    RecyclerView photo_grid;
    FastItemAdapter<PhotoItem> fastAdapter;
    String [] url_array;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_detail);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);

        photo_grid = (RecyclerView) findViewById(R.id.photos_grid);
        photo_grid.setLayoutManager(gridLayoutManager);

        fastAdapter = new FastItemAdapter<>();

        album_id = getIntent().getIntExtra("album_id", -1);

        // photos.get by album_id

        VKRequest requestPhotos = new VKRequest("photos.get", VKParameters.from("album_id", album_id));

        requestPhotos.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);

                JSONObject jsonResponse = response.json;
                try {
                    photo_count = jsonResponse.getJSONObject("response").getInt("count");
                    for (int i = 0; i < photo_count; i++) {
                        fastAdapter.add(new PhotoItem()
                                .withName("Loading..")
                                .withIsLoad(true));
                    }
                    photo_grid.setAdapter(fastAdapter);

                    new getPhotos().execute(jsonResponse);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(VKError error) {
                super.onError(error);
            }
        });

    }


    private class getPhotos extends AsyncTask<JSONObject, String, VKApiPhoto[]> {
        @Override
        protected VKApiPhoto[] doInBackground(JSONObject... response) {

            VKApiPhoto [] photos =  new VKApiPhoto[photo_count];

            try {
                JSONArray jsonPhotos = response[0].getJSONObject("response").getJSONArray("items");
                for (int i = 0; i < photo_count; i++) {
                    JSONObject item = jsonPhotos.getJSONObject(i);
                    photos[i] = new VKApiPhoto(item);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return photos;
        }

        @Override
        protected void onPostExecute(final VKApiPhoto[] photos){

            fastAdapter.clear();

            url_array = new String[photos.length];

            for (int i = 0; i < photos.length; i++) {
                VKApiPhoto photo = photos[i];
                int id = photo.id;
                String name = photo.text;
                long unix_time = photo.date;
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
                String upload_date = dateFormat.format(unix_time * 1000);
                String imgUrl = photo.photo_604;
                fastAdapter.add(new PhotoItem().withId(id)
                        .withName(name)
                        .withDate(upload_date)
                        .withImage(imgUrl)
                        .withIsLoad(false));
                url_array[i] = getMaxPhotoSizeUrl(photo);
            }

            photo_grid.setAdapter(fastAdapter);

            fastAdapter.withOnClickListener(new FastAdapter.OnClickListener<PhotoItem>() {
                @Override
                public boolean onClick(View v, IAdapter<PhotoItem> adapter, PhotoItem item, int position) {
                    Intent view_photo = new Intent(AlbumDetailActivity.this, PhotoViewActivity.class);
                    view_photo.putExtra("position", position);
                    view_photo.putExtra("url_array", url_array);
                    startActivity(view_photo);
                    return true;
                }
            });

        }
    }

    String getMaxPhotoSizeUrl(VKApiPhoto photo) {

        if (photo.photo_2560 != null) {
            if (!photo.photo_2560.isEmpty()) {
                return photo.photo_2560;
            }
        }

        if (photo.photo_1280 != null) {
            if (!photo.photo_1280.isEmpty()) {
                return photo.photo_1280;
            }
        }

        if (photo.photo_807 != null) {
            if (!photo.photo_807.isEmpty()) {
                return photo.photo_807;
            }
        }

        if (photo.photo_604 != null) {
            if (!photo.photo_604.isEmpty()) {
                return photo.photo_604;
            }
        }

        if (photo.photo_130 != null) {
            if (!photo.photo_130.isEmpty()) {
                return photo.photo_130;
            }
        }

        if (photo.photo_75 != null) {
            if (!photo.photo_75.isEmpty()) {
                return photo.photo_75;
            }
        }

        return "";
    }

}
